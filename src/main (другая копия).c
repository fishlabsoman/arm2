
#include "stm32l1xx.h"
#include "stm32l1xx_conf.h"
//#include "stm32l1xx_usart.h"

#include <string.h>



void Delay_ms(uint32_t ms) {
	volatile uint32_t nCount;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq ( & RCC_Clocks);

	nCount = (RCC_Clocks.HCLK_Frequency / 10000) * ms;
	for (; nCount != 0; nCount--);
}

void Sleep (void) {///wait for interrupt
	__ASM("wfi");
}

#define LINEMAX 100 // Maximal allowed/expected line length

volatile char line_buffer[] = "qwertyuiop[]asdfghjkl;zxcvbnm,./qwertyuiop[]asdfghjkl;zxcvbnm,./";
volatile int line_valid = 0;


volatile char StringLoop[] = "The quick brown fox jumps over the lazy dog\r\n";

volatile char tx_valid = 0;
volatile char rx_valid = 0;
//**************************************************************************************


// Буфер на прием
#define RX_BUFFER_SIZE 350 // размер буфера
volatile uint8_t    rx_buffer[RX_BUFFER_SIZE];
volatile uint16_t   rx_wr_index=0, //индекс хвоста буфера (куда писать данные)
                    rx_rd_index=0, //индекс начала буфера (откуда читать данные)
                    rx_counter=0; //количество данных в буфере
volatile uint8_t    rx_buffer_overflow=0; //информация о переполнении буфера

// Буфер на передачу
#define TX_BUFFER_SIZE 350 //размер буфера
volatile uint8_t   tx_buffer[TX_BUFFER_SIZE];
volatile uint16_t  tx_wr_index=0, //индекс хвоста буфера (куда писать данные)
                   tx_rd_index=0, //индекс начала буфера (откуда читать данные)
                   tx_counter=0; //количество данных в буфере




int main(void) {

	// enable the GPIOC/B peripheral
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	// configure pins 6 and 7 as GPIO output
	GPIO_InitTypeDef speaker_GPIO;
	speaker_GPIO.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_8;
	speaker_GPIO.GPIO_Mode = GPIO_Mode_OUT;
	// initialize the peripheral
	GPIO_Init(GPIOC, & speaker_GPIO);
	//GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	//Delay_ms(100);

	//GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );


	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); //Разрешаем тактирование
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE); //???

	//назначаем альтернативные функции
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART1); //PD5 to TX USART2
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART1); //PD6 to RX USART2


	//заполняем поля структуры
	// PA9 -> TX UART.
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure); //инициализируем

	//PA10  -> RX UART.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA, &GPIO_InitStructure);//инициализируем


	USART_InitTypeDef USART_InitStructure;

  /* Стандартные настройки COM порта ПК----------------------------------*/
  /* USART2 configured as follow:
  - BaudRate = 9600 baud
  - Word Length = 8 Bits
  - One Stop Bit
  - No parity
  - Hardware flow control disabled (RTS and CTS signals)
  - Receive and transmit enabled
  */
  USART_InitStructure.USART_BaudRate = 1200;// скорость
  USART_InitStructure.USART_WordLength = USART_WordLength_8b; //8 бит данных
  USART_InitStructure.USART_StopBits = USART_StopBits_1; //один стоп бит
  USART_InitStructure.USART_Parity = USART_Parity_No; //четность - нет
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // управлени потоком - нет
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;       // разрешаем прием и передачу

  //USART_OverSampling8Cmd(ENABLE); //можно уменьшить частоту семплирования
//USART_OneBitMethodCmd(ENABLE); //можно уменьшить количество стробирований
//USART_HalfDuplexCmd (ENABLE); // можно выбрать полудуплексный режим.

  USART_Init(USART1, &USART_InitStructure); //инизиализируем

  USART_ClockInitTypeDef USART_ClockInitStructure;
  USART_ClockInitStructure.USART_Clock=USART_Clock_Enable;
  USART_ClockInitStructure.USART_CPHA=USART_CPHA_1Edge;
  USART_ClockInitStructure.USART_CPOL=USART_CPOL_High;
  USART_ClockInitStructure.USART_LastBit=USART_LastBit_Enable;
  USART_ClockInit(USART2, &USART_ClockInitStructure);

char data = '0';




NVIC_InitTypeDef NVIC_InitStructure;
  /* Configure the Priority Group to 2 bits */
NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //конфигурируем количество групп и подгрупп прерываний, пока у нас одно прерывание нам это особо ничего не дает

  /* Enable the USARTx Interrupt */
NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn; //прерывание по uart2
NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; //задаем приоритет в группе
NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; //задаем приоритет в подгруппе
NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //разрешаем прерывание
NVIC_Init(&NVIC_InitStructure); //инициализируем



 USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
  USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
USART_Cmd(USART2, ENABLE);

	tx_valid = 1;






	while (1) {
GPIO_ToggleBits(GPIOC, GPIO_Pin_8 );

		//line_buffer = "asdasddasdsaddas\n\0";
		tx_valid = 1;
		//*/
		Delay_ms(500);

			GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}
}

//**************************************************************************************


/*
void USART1_IRQHandler(void) {
    //GPIO_SetBits(GPIOC, GPIO_Pin_13 );
    static int tx_index = 0;
    static int rx_index = 0;
    static char rx_buffer[LINEMAX+1];

    if (tx_valid) {

        if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET) { // Transmit the string in a loop
            USART_SendData(USART1, StringLoop[tx_index++]);

            if (tx_index >= 10) {
                tx_index = 0;

                tx_valid = 0;
            }
        }
    }




    //static int rx_index = 0;

    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) { // Received character?
        char rx =  USART_ReceiveData(USART1);

        if ((rx == '\r') || (rx == '\n')) { // Is this an end-of-line condition, either will suffice?
            if (rx_index != 0) { // Line has some content?
                memcpy((void * )line_buffer, rx_buffer, rx_index); // Copy to static line buffer from dynamic receive buffer
                line_buffer[rx_index] = 0; // Add terminating NUL
                tx_valid = 1; // flag new line valid for processing

                rx_index = 0; // Reset content pointer
            }
        } else if (rx_index == LINEMAX) {
            rx_index = 0;
        }else{
            rx_buffer[rx_index++] = rx; // Copy to buffer and increment
        }
    }
}
//*/

int volatile first = 1;

void USART2_IRQHandler(void) {
/*
	USART1->SR &= ~USART_SR_RXNE; //сброс флага
	USART1->SR &= ~USART_SR_TXE; //сброс флага



	if (USART_SR_RXNE){
		first++;
	}
	if (USART_SR_TXE){
		first++;
	}


USART1->SR&=~(USART_SR_TXE|USART_SR_RXNE); //очистка флагов



*/



/*
	if(first == 1){
		first = 0;
		return;
	}

    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_Init(&NVIC_InitStructure);

	static int tx_index = 0;
	static int rx_index = 0;

	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET) { // Transmit the string in a loop
		USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
		if (tx_valid ) {
			//USART_SendData(USART1, StringLoop[tx_index++]);

			if (tx_index >= (sizeof(StringLoop) - 1)) {
				tx_index = 0;
			}
		}
		USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
		//return;
	}


	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) { // Received characters modify string
		USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		StringLoop[rx_index++] = USART_ReceiveData(USART1);

		if (rx_index >= (sizeof(StringLoop) - 1)){
			rx_index = 0;
		}
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	}
	*/

	GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	Delay_ms(10);


}




void RTC_Alarm_IRQHandler(void){

}



//**************************************************************************************

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t * file, uint32_t line) {
	/* User can add his own implementation to report the file name and line number,
	   ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1) {
	}
}

#endif







//**************************************************************************************

/*
void USART1_IRQHandler(void) {
    //GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
    static char rx_buffer[LINEMAX];   // Local holding buffer to build line
    static int rx_index = 0;

    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) { // Received character?
        char rx =  USART_ReceiveData(USART1);

        if ((rx == '\r') || (rx == '\n')) { // Is this an end-of-line condition, either will suffice?
            if (rx_index != 0) { // Line has some content?
                memcpy((void * )line_buffer, rx_buffer, rx_index); // Copy to static line buffer from dynamic receive buffer
                line_buffer[rx_index] = 0; // Add terminating NUL
                line_valid = 1; // flag new line valid for processing

                rx_index = 0; // Reset content pointer
            }
        } else if (rx_index == LINEMAX) {
            rx_index = 0;
        }

        rx_buffer[rx_index++] = rx; // Copy to buffer and increment

    }
}


#ifdef DEBUG
void __error__(char * pcFilename, unsigned long ulLine) {
    //
    // Something horrible happened! You need to look
    // at file "pcFilename" at line "ulLine" to see
    // what error is being reported.
    //
    while (1) {
    }
}
#endif

#ifdef  USE_FULL_ASSERT


void assert_failed(uint8_t * file, uint32_t line) {
    while (1) {
    }
}
#endif
*/