
#include "stm32l1xx.h"
#include "stm32l1xx_conf.h"
//#include "stm32l1xx_usart.h"

#include <string.h>


//delay milliseconds
void delay(uint32_t ms) {
	volatile uint32_t nCount;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq ( & RCC_Clocks);

	nCount = (RCC_Clocks.HCLK_Frequency / 10000) * ms;
	for (; nCount != 0; nCount--);
}

//sleep to external interrupt
void sleep (void) {///wait for interrupt
	__ASM("wfi");
}


volatile char tmpString[255];
volatile char needSend = 0;


int main(void) {


	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);//enable GPIOC

	//config pin for output // need for debug
	GPIO_InitTypeDef speaker_GPIO;
	speaker_GPIO.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_8;
	speaker_GPIO.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIOC, & speaker_GPIO);



	//declare needed structures
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;


	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);//enable clocking PAX
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);//enable clocking UART1

	//associate pins with usart
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);


	//Configure USART1 pins Rx and Tx
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, & GPIO_InitStructure);

	/* Enable USART1 IRQ */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;//very high priority
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( & NVIC_InitStructure);

	//setup uart
	USART_InitStructure.USART_BaudRate = 1200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, & USART_InitStructure);


	USART_Cmd(USART1, ENABLE);//enable


	//setup interrupts
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART1, USART_IT_TXE, ENABLE);//enable if only needed transfer and disable after



	int i = 0;
	while (1) {

		if (i == 4) {
			i = 0;
			if(needSend == 1) continue;//reset while if sending
			needSend = 1;

			USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		}
		i++;
		GPIO_ToggleBits(GPIOC, GPIO_Pin_8 );
		delay(700);

	}

}

//**************************************************************************************


//TODO: NEED BUFER!
//if using cpp -> extern "C" void USART1_IRQHandler(void)
//using needSend flag for tx
void USART1_IRQHandler(void) {

	//TODO: need good circle buffer
	static int tx_index = 0;
	static int rx_index = 0;
	static char rx = '\0';
	static char tx = '\0';

	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET) {//tx send interrupt
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );//for lulz

		if (needSend == 1) {

			tx = tmpString[tx_index];

			if (tx == '\r' || tx == '\0') {//check for "endline" simbols and replace to \n
				tx = '\n';
			}

			USART_SendData(USART1, tx);

			if (tx == '\n' tx_index >= 255) {
				tx_index = 0;
				needSend = 0;
			} else {
				tx_index++;
			}

		} else { //else disable tx. //if tx flag always enabled USART1_IRQHandler is
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		}
	}//end if TXE

	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {//

		rx = USART_ReceiveData(USART1);
		tmpString[rx_index] = rx;

		if (rx == '\n' || rx == '\r' || rx == '\0' || rx_index >= 255) {//endline
			rx_index = 0;
		} else {
			rx_index++;
		}
	}//end if rxne



	//TODO: а RESET ли?!
	if (USART_GetITStatus(USART1, USART_IT_ORE_RX) != RESET) { //Прерывание по переполнении при установленном бите RXNEIE
		rx = USART_ReceiveData(USART1);//for skip error
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}

	if (USART_GetITStatus(USART1, USART_IT_ORE_ER) != RESET) { //Прерывание по переполнении при установленном бите EIE
		rx = USART_ReceiveData(USART1);//for skip error
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}


	//other flags
	if (USART_GetITStatus(USART1, USART_IT_TC) != RESET) {//передача закончена
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}

	if (USART_GetITStatus(USART1, USART_IT_IDLE) != RESET) { //Idle line detection interrupt.
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}

	if (USART_GetITStatus(USART1, USART_IT_ERR) != RESET) { //Прерывание по факту ошибки
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}

	if (USART_GetITStatus(USART1, USART_IT_LBD) != RESET) { //LIN Break detection interrupt.
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}

	if (USART_GetITStatus(USART1, USART_IT_CTS) != RESET) { //Прерывание по изменению состояния CTS
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}


	if (USART_GetITStatus(USART1, USART_IT_NE) != RESET) { //прерывание из-за «шума» на линии
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}

	if (USART_GetITStatus(USART1, USART_IT_FE) != RESET) { //Прерывание из-за ошибки пакета
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}

	if (USART_GetITStatus(USART1, USART_IT_PE) != RESET) {//ошибка четности
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	}
}//end of USART1_IRQHandler


//хз что это :D
#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t * file, uint32_t line) {
	/* User can add his own implementation to report the file name and line number,
	   ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1) {
	}
}
#endif