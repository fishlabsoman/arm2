
#include "stm32l1xx.h"
#include "stm32l1xx_conf.h"

#include <string.h>

#define LINEMAX 100 // Maximal allowed/expected line length

volatile char line_buffer[LINEMAX + 1]; // Holding buffer with space for terminating NUL
volatile int line_valid = 0;

//*/


//using namespace std;


void Delay_ms(uint32_t ms) {
	volatile uint32_t nCount;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq ( & RCC_Clocks);

	nCount = (RCC_Clocks.HCLK_Frequency / 10000) * ms;
	for (; nCount != 0; nCount--);
}

void Delay_ns(uint32_t ns) {
	volatile uint32_t nCount;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq ( & RCC_Clocks);

	nCount = (RCC_Clocks.HCLK_Frequency / 10000000) * ns;
	for (; nCount != 0; nCount--);
}



void sendData(char *  data) {

	//USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);

	//

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
	NVIC_Init( & NVIC_InitStructure);
//*/
	int maxIterations = 1024;
	int i = 0;


	int size = 29 ;

	while(i < size){
		//if(--maxIterations < 0)break;
		if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET) {
			USART_SendData(USART1, data[i]);
			i++;
		}
	} //while (data[i] != '\0');

	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( & NVIC_InitStructure);

	//USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
	//USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	//*/
}







//**************************************************************************************
USART_InitTypeDef USART_InitStructure;
int main(void) {


	///__enable_irq(); //Глобальное включение прерывания

	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);



	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, & GPIO_InitStructure);


	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	//NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( & NVIC_InitStructure);
//*/

	USART_InitStructure.USART_BaudRate = 1200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, & USART_InitStructure);

	USART_Cmd(USART1, ENABLE);


	USART_ITConfig(USART1, USART_IT_RXNE|USART_IT_TXE, ENABLE);
	//USART_ITConfig(USART1, USART_IT_TXE, ENABLE);

//*/

/*

	// enable the GPIOC/B peripheral
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	// configure pins 6 and 7 as GPIO output
	GPIO_InitTypeDef speaker_GPIO;
	speaker_GPIO.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_8;
	speaker_GPIO.GPIO_Mode = GPIO_Mode_OUT;
	// initialize the peripheral
	GPIO_Init(GPIOC, & speaker_GPIO);

	GPIO_SetBits(GPIOC, GPIO_Pin_13 );

*/
	while (1) {


		if (!line_valid) { // A new line has arrived
			// ProcessLine(line_buffer); // Do something with the line
			//GPIO_ToggleBits(GPIOC, GPIO_Pin_8 );
			sendData("\nqwertyuiopasdfghjklzxcvbnm\n");
			line_valid = 0; // clear pending flag
		}
//*/
		Delay_ms(1000);


	}
}

//**************************************************************************************

void USART1_IRQHandler(void) {
	//GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
	static char rx_buffer[LINEMAX];   // Local holding buffer to build line
	static int rx_index = 0;

	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) { // Received character?
		char rx =  USART_ReceiveData(USART1);

		if ((rx == '\r') || (rx == '\n')) { // Is this an end-of-line condition, either will suffice?
			if (rx_index != 0) { // Line has some content?
				memcpy((void * )line_buffer, rx_buffer, rx_index); // Copy to static line buffer from dynamic receive buffer
				line_buffer[rx_index] = 0; // Add terminating NUL
				line_valid = 1; // flag new line valid for processing

				rx_index = 0; // Reset content pointer
			}
		} else if (rx_index == LINEMAX) {
			rx_index = 0;
		}

		rx_buffer[rx_index++] = rx; // Copy to buffer and increment

	}
}
//*/
//**************************************************************************************


#ifdef DEBUG
void __error__(char * pcFilename, unsigned long ulLine) {
	//
	// Something horrible happened! You need to look
	// at file "pcFilename" at line "ulLine" to see
	// what error is being reported.
	//
	while (1) {
	}
}
#endif

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t * file, uint32_t line) {
	/* User can add his own implementation to report the file name and line number,
	   ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1) {
	}
}
#endif
