#include "stm32l1xx.h"

#include "stm32l1xx_usart.c"



char uart2_rx_buf[128]; 
uint8_t uart2_rx_bit; 


void usart_init(void) {
	GPIO_InitTypeDef GPIO_InitStructure; //Структура содержащая настройки порта
	USART_InitTypeDef USART_InitStructure; //Структура содержащая настройки USART

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE); //Включаем тактирование порта A
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); //Включаем тактирование порта USART2

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2); //Подключаем PA3 к TX USART2
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2); //Подключаем PA2 к RX USART2

	//Конфигурируем PA2 как альтернативную функцию -> TX UART. Подробнее об конфигурации можно почитать во втором уроке.
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	//GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, & GPIO_InitStructure);

	//Конфигурируем PA3 как альтернативную функцию -> RX UART. Подробнее об конфигурации можно почитать во втором уроке.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA, & GPIO_InitStructure);

	USART_StructInit( & USART_InitStructure); //Инициализируем UART с дефолтными настройками: скорость 9600, 8 бит данных, 1 стоп бит

	USART_Init(USART2, & USART_InitStructure);
	USART_Cmd(USART2, ENABLE); //Включаем UART
}


void Delay_ms(uint32_t ms) {
	volatile uint32_t nCount;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq ( & RCC_Clocks);

	nCount = (RCC_Clocks.HCLK_Frequency / 10000) * ms;
	for (; nCount != 0; nCount--);
}

void Delay_ns(uint32_t ns) {
	volatile uint32_t nCount;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq ( & RCC_Clocks);

	nCount = (RCC_Clocks.HCLK_Frequency / 10000000) * ns;
	for (; nCount != 0; nCount--);
}




//Функция отправляет байт в UART
void send_to_uart(uint8_t data) {
	while (!(USART2->SR & USART_SR_TC));
	USART2->DR = data;
}

//Функция отправляет строку в UART, по сути пересылая по байту в send_to_uart
void send_str(char * string) {
	uint8_t i = 0;
	while (string[i]) {
		send_to_uart(string[i]);
		i++;
	}
}


void USART2_IRQHandler (void) {
	char uart_data;
	if (USART2->SR & USART_SR_RXNE) { //Проверяем, прило ли чтонибудь в UART
		char uart2_rx_buf[255];
		USART2->DR = USART2->DR; //Echo по приему, символ отправленный в консоль вернется
		uart_data = USART2->DR; //Считываем то что пришло в переменную...
		uart2_rx_buf[uart2_rx_bit] = USART2->DR; //Помещаем принятый байт в буфер.
		uart2_rx_bit++; //Наращиваем счётчик байтов буфера.

		if (uart_data == '\r') { //Если пришло сообщение о нажатии Enter...
			GPIO_ResetBits(GPIOD, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15); //Сбрасываем все пины в «0»
			if (strcmp(uart2_rx_buf, "led0\r") == 0) { //Если пришла команда "led0"
				GPIO_SetBits(GPIOD, GPIO_Pin_12); //Подаем «1» на PD12
			} else if (strcmp(uart2_rx_buf, "led1\r") == 0) { //Если пришла команда "led1"
				GPIO_SetBits(GPIOD, GPIO_Pin_13); //Подаем «1» на PD13
			} else if (strcmp(uart2_rx_buf, "led2\r") == 0) { //Если пришла команда "led2"
				GPIO_SetBits(GPIOD, GPIO_Pin_14); //Подаем «1» на PD14
			} else if (strcmp(uart2_rx_buf, "led3\r") == 0) { //Если пришла команда "led3"
				GPIO_SetBits(GPIOD, GPIO_Pin_15); //Подаем «1» на PD15
			} else {
				send_str("\n"); //Переходим на новую строку
				send_str("String: ");
				send_str(uart2_rx_buf); //Отправляем ее обратно в консоль
			}

			memset(uart2_rx_buf, 0, sizeof(uart2_rx_buf)); //Очищаем буфер
			uart2_rx_bit = 0; //Сбрасываем счетчик
			send_str("\n");
		}
	}
}


int main() {
	GPIO_InitTypeDef speaker_GPIO;


	// enable the GPIOC/B peripheral
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	// configure pins 6 and 7 as GPIO output
	speaker_GPIO.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_4;
	speaker_GPIO.GPIO_Mode = GPIO_Mode_OUT;
	// initialize the peripheral
	GPIO_Init(GPIOC, & speaker_GPIO);


	GPIO_InitTypeDef hl_GPIO;
	hl_GPIO.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
	hl_GPIO.GPIO_Mode = GPIO_Mode_OUT;
	// initialize the peripheral
	GPIO_Init(GPIOB, & hl_GPIO);


	GPIO_ResetBits(GPIOC, GPIO_Pin_13);//disable speaker
	GPIO_SetBits(GPIOC, GPIO_Pin_8);//enable +24v relay

	GPIO_ResetBits(GPIOB, GPIO_Pin_5 | GPIO_Pin_4 | GPIO_Pin_3); //5 LE, 4 CLK, 3 DATA  //reset all pins





	usart_init(); //Инициализируем UART
	//Настраиваем прерывания по приему
	__enable_irq(); //Глобальное включение прерывания
	NVIC_EnableIRQ(USART2_IRQn); //Включаем прерывания от UART
	NVIC_SetPriority(USART2_IRQn, 0); //Прерывание от UART, приоритет 0, самый высокий
	USART2->CR1 |= USART_CR1_RXNEIE; //Прерывание по приему



	GPIO_InitTypeDef GPIO_InitStructure; //Структура содержащая настройки порта
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE); //Включаем тактирование порта D
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15; //Выбераем нужные вывод
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; //Включаем режим выхода
	GPIO_Init(GPIOD, & GPIO_InitStructure); //вызов функции







	char SEGMENTE2[11];

	SEGMENTE2[0] = 0xEE;//+++++     //0xEF 0.
	SEGMENTE2[1] = 0x06;//+++++     //0x07 1.
	SEGMENTE2[2] = 0xBA;//+++++     //0xBB 2.
	SEGMENTE2[3] = 0x3E;//+++++     //0x3F 3.
	SEGMENTE2[4] = 0x56;//+++++     //0x57 4.
	SEGMENTE2[5] = 0x7C;//+++++     //0x7D 5.
	SEGMENTE2[6] = 0xFC;//+++++     //0xFE 6.
	SEGMENTE2[7] = 0x4E;//+++++     //0x4F 7.
	SEGMENTE2[8] = 0xFE;//+++++     //0xFF 8.
	SEGMENTE2[9] = 0x7E;//++++++    //0x7F 9.
	SEGMENTE2[10] = 0x00;//EMPTY
	SEGMENTE2[11] = 0x00 | 0x01; //DOT //example adding dot

	// Функция вывода 1 разряда

	char data = 0;
	char m;

	char seg = 0;
	while (1) {
		if (seg > 11)seg = 0;
		Delay_ms(100);

		GPIO_SetBits(GPIOB, GPIO_Pin_5);//enable pin
		__ASM("nop");
		GPIO_ResetBits(GPIOB, GPIO_Pin_5);


		data = SEGMENTE2[seg];

		for (m = 0; m < 8 ; m++) {
			if ((data & 0x80) != 0) { // Сравниваем 8-й бит с нулем
				GPIO_ResetBits(GPIOB, GPIO_Pin_3);//DATA
			} else {
				GPIO_SetBits(GPIOB, GPIO_Pin_3);//DATA
			}

			GPIO_ResetBits(GPIOB, GPIO_Pin_4);// CLK 0
			__ASM("nop");
			GPIO_SetBits(GPIOB, GPIO_Pin_4); // CLK 1
			data = data << 1; // Сдвигаем биты
		}
		seg++;
	}

	// loop forever
	while (1) {
		// toggle pins 6 and 7
		GPIO_ToggleBits(GPIOC, GPIO_Pin_13 );
		GPIO_ToggleBits(GPIOC, GPIO_Pin_4 );
		// waste time
		Delay_ms(1000);
	}
	return 0;
}


